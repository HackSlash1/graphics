var path = require('path');
const webpack = require('webpack');

module.exports = {
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
		compress: true,
		host: '0.0.0.0',
		public: 'hacklaplinux.local:8000',
		port: 8000
	},
	plugins:[
	new webpack.IgnorePlugin(/(fs)/)
	]
};