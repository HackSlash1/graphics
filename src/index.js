'use strict';
import * as Ammo from "ammo.js"
import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

let physicsWorld, container, scene, camera, clock, renderer, controls, rigidBodies = [], tmpTrans;
let colGroupPlane = 1, colGroupRedBall = 2, colGroupGreenBall = 4;

// CREATES THE FLOOR
function createBlock(){
    // block variables
    let pos = {x: 0, y: 0, z: 0};
    let scale = {x: 50, y: 2, z: 50};
    let rot = {x: 0, y: 0, z: 0, w: 1};
    let mass = 0;

    // GRAPHICS SECTION
    let blockPlane = new THREE.Mesh(new THREE.BoxBufferGeometry(), new THREE.MeshPhongMaterial({color: 'grey'}));

    blockPlane.position.set(pos.x, pos.y, pos.z);
    blockPlane.scale.set(scale.x, scale.y, scale.z);
    scene.add(blockPlane);

    // PHYSICS SECTION
    let transform = new Ammo.btTransform();
    transform.setIdentity();
    transform.setOrigin( new Ammo.btVector3( pos.x, pos.y, pos.z ) );
    transform.setRotation( new Ammo.btQuaternion( rot.x, rot.y, rot.z, rot.w ) );
    let motionState = new Ammo.btDefaultMotionState( transform );

    // Set collision shape to body shape
    let colShape = new Ammo.btBoxShape( new Ammo.btVector3( scale.x * 0.5, scale.y * 0.5, scale.z * 0.5 ) );
    colShape.setMargin( 0.05 );

    // Apply mass
    let localInertia = new Ammo.btVector3( 0, 0, 0 );
    colShape.calculateLocalInertia( mass, localInertia );

    // apply all info to body
    let rbInfo = new Ammo.btRigidBodyConstructionInfo( mass, motionState, colShape, localInertia );
    let body = new Ammo.btRigidBody( rbInfo );
    body.castShadow = true;
    body.receiveShadow = true;

    physicsWorld.addRigidBody( body, colGroupPlane, colGroupRedBall );
}

function createBall(){
    
    // ball variables
    let pos = {x: 0, y: 20, z: 0};
    let rad = 2;
    let rot = {x: 0, y: 0, z: 0, w: 1};
    let mass = 1;

    //GRAPHICS SECTION
    var geometry = new THREE.SphereBufferGeometry(rad);
    var vertexDisplacement = new Float32Array(geometry.attributes.position.count);
    for (var i = 0; i < vertexDisplacement.length; i++) {
        vertexDisplacement[i] = Math.sin(i);
    }
    geometry.setAttribute('vertexDisplacement', new THREE.BufferAttribute(vertexDisplacement, 1));
    
    var texture = new THREE.TextureLoader().load( 'textures/Football.jpg' );

    var uniforms = {
        u_texture: { type: "t", value: texture }
    };

    var material = new THREE.ShaderMaterial({
        uniforms: uniforms,
        vertexShader: document.getElementById('vertexShader').textContent,
        fragmentShader: document.getElementById('fragmentShader').textContent
    });
    
    let ball = new THREE.Mesh(geometry, material);

    ball.position.set(pos.x, pos.y, pos.z);
    ball.castShadow = true;
    ball.receiveShadow = true;

    scene.add(ball);


    //PHYSICS SECTION
    let transform = new Ammo.btTransform();
    transform.setIdentity();
    transform.setOrigin( new Ammo.btVector3( pos.x, pos.y, pos.z ) );
    transform.setRotation( new Ammo.btQuaternion( rot.x, rot.y, rot.z, rot.w ) );
    let motionState = new Ammo.btDefaultMotionState( transform );

    let colShape = new Ammo.btSphereShape( rad );
    colShape.setMargin( 0.05 );

    let localInertia = new Ammo.btVector3( 0, 0, 0 );
    colShape.calculateLocalInertia( mass, localInertia );

    let rbInfo = new Ammo.btRigidBodyConstructionInfo( mass, motionState, colShape, localInertia );
    let body = new Ammo.btRigidBody( rbInfo );


    physicsWorld.addRigidBody( body, colGroupRedBall, colGroupPlane | colGroupGreenBall );
    
    ball.userData.physicsBody = body;
    rigidBodies.push(ball);
}

function createMaskBall(){
    
    // maskball variables
    let pos = {x: 1, y: 30, z: 0};
    let rad = 2;
    let quat = {x: 0, y: 0, z: 0, w: 1};
    let mass = 1;

    //GRAPHICS SECTION

    let ball = new THREE.Mesh(new THREE.SphereBufferGeometry(rad), new THREE.MeshPhongMaterial({color: '#00ff08'}));

    ball.position.set(pos.x, pos.y, pos.z);
    ball.castShadow = true;
    ball.receiveShadow = true;

    scene.add(ball);


    //PHYSICS SECTION
    let transform = new Ammo.btTransform();
    transform.setIdentity();
    transform.setOrigin( new Ammo.btVector3( pos.x, pos.y, pos.z ) );
    transform.setRotation( new Ammo.btQuaternion( quat.x, quat.y, quat.z, quat.w ) );
    let motionState = new Ammo.btDefaultMotionState( transform );

    let colShape = new Ammo.btSphereShape( rad );
    colShape.setMargin( 0.05 );

    let localInertia = new Ammo.btVector3( 0, 0, 0 );
    colShape.calculateLocalInertia( mass, localInertia );

    let rbInfo = new Ammo.btRigidBodyConstructionInfo( mass, motionState, colShape, localInertia );
    let body = new Ammo.btRigidBody( rbInfo );


    physicsWorld.addRigidBody( body, colGroupGreenBall, colGroupRedBall);
    
    ball.userData.physicsBody = body;
    rigidBodies.push(ball);
}

function update() {
    let deltaTime = clock.getDelta();
    updatePhysics( deltaTime );
    renderer.render( scene, camera );
    requestAnimationFrame( update );
};

// Resize fix
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
}

function updatePhysics( deltaTime ){

    // Step world
    physicsWorld.stepSimulation( deltaTime, 10 );

    // Update rigid bodies
    for ( let i = 0; i < rigidBodies.length; i++ ) {
        let objThree = rigidBodies[ i ];
        let objAmmo = objThree.userData.physicsBody;
        let ms = objAmmo.getMotionState();
        if ( ms ) {
            ms.getWorldTransform( tmpTrans );
            let p = tmpTrans.getOrigin();
            let q = tmpTrans.getRotation();
            objThree.position.set( p.x(), p.y(), p.z() );
            objThree.quaternion.set( q.x(), q.y(), q.z(), q.w() );

        }
    }

}

function initPhysics() {
    var collisionConfiguration = new Ammo.btDefaultCollisionConfiguration();
    var dispatcher = new Ammo.btCollisionDispatcher( collisionConfiguration );
    var broadphase = new Ammo.btDbvtBroadphase();
    var solver = new Ammo.btSequentialImpulseConstraintSolver();

    physicsWorld = new Ammo.btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
    physicsWorld.setGravity(new Ammo.btVector3(0, -10, 0));
}

function initLight() {
    // AMBIENT_LIGHT
    let ambient = new THREE.AmbientLight('#ffffff', 0.5);
    scene.add(ambient);

    // DIRECTIONAL LIGHT
    let directionalLight = new THREE.DirectionalLight('#ffffff', 0.5);
    directionalLight.position.x = -1000;
    directionalLight.position.y = 1000;
    directionalLight.position.z = 1000;
    scene.add(directionalLight);

    // SPOTLIGHT

    let light = new THREE.SpotLight('blue', 1.3); //'#efdfd5'
    light.position.y = 100;
    light.target.position.set(0,0,0);
    light.castShadow = true;
    light.shadow.camera.near = 50;
    light.shadow.camera.far = 110;
    light.shadow.mapSize.width = 1024;
    light.shadow.mapSize.height = 1024;
    scene.add(light);
}

function init() {
    // CONTAINER
    container = document.getElementById("ThreeJS");

    // SCENE
    scene = new THREE.Scene();
    //scene.background = new THREE.Color( 'grey' ); //'#bfd1e5'

    // CAMERA
    var FOV = 45, ASPECT = window.innerWidth / window.innerHeight, NEAR = 0.01, FAR = 20000;
    camera = new THREE.PerspectiveCamera( FOV, ASPECT, NEAR, FAR );
    scene.add(camera)
    camera.position.set( 0, 30, 70 );
    camera.lookAt(new THREE.Vector3(0, 0, 0));

    // CLOCK
    clock = new THREE.Clock();

    // RENDERER
    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    renderer.shadowMap.enabled = true;
    renderer.outputEncoding = THREE.sRGBEncoding;
    container.appendChild(renderer.domElement);

    // CONTROLS
    controls = new OrbitControls(camera,renderer.domElement);

    // LIGHT
    initLight()

    // PHYSICS
    initPhysics();

    // FLOOR
    createBlock();
    window.addEventListener( 'resize', onWindowResize, false);
}

// Start program
Ammo(Ammo).then( function() {
    tmpTrans = new Ammo.btTransform();
    init();
    createBall();
    createMaskBall();
    update();

} );

